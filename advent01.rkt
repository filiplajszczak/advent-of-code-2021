#lang racket
(require algorithms)

(define (input filename) (map string->number (file->lines filename)))

(define (sums-of-sliding-triads lst)
  (zip-with + (init (init lst)) (init (cdr lst)) (cddr lst)))

(define (part-1 filename)
  (count identity (adjacent-map < (input filename))))

(define (part-2 filename)
  (count identity (adjacent-map < (sums-of-sliding-triads (input filename)))))

;; Part 1
(display (part-1 "day01.in"))
(newline)
;; Part 2
(display (part-2 "day01.in"))
(newline)

;; Tests
(require rackunit)

(check-equal? 7 (part-1 "day01-example.in"))
(check-equal? 5 (part-2 "day01-example.in"))

